//
//  ApiList.swift
//  Cosmopolitan club
//
//  Created by Chan Basha Shaik on 07/06/19.
//  Copyright © 2019 Vinod Reddy Sure. All rights reserved.
//

import Foundation

enum Api : String{
    case home = "Home"
    case memberLogIn = "Member Login"
    case billSummary = "Bill Summary"
    case paymentHistory = "Payments History"
    case bookings = "Bookings"
    case library = "Library"
    case login = "/ip.jsontest.com/"
    case DiningList = "/api/DiningList"
    case BanquetList = "/api/BanquetList"
    case FacilityList = "/api/FacilityList"
    case SportsList = "/api/SportsList"
    case GolfList = "/api/GolfList"
    case EventsList = "/api/EventsList"
    case PromotionsList = "/api/PromotionsList"
    case GalleryList = "/api/GalleryList"
    case Notifications = "Notifications"
    case DiningDetails = "/api/DiningDetails?id="
    case DiningCuisine = "/api/DiningCuisine?id="
    case DiningImages = "/api/DiningImages?id="
    
    case imageBaseUrl = "https:cosmopolitanclubchennai.com/images/food_dining/"
}

//Http Method Types

enum HttpType : String{
    
    case GET = "GET"
    case POST = "POST"
    case PATCH = "PATCH"
    case PUT = "PUT"
    case DELETE = "DELETE"
    
}

// Status Code

enum StatusCode : Int {
    
    case notreachable = 0
    case success = 200
    case multipleResponse = 300
    case unAuthorized = 401
    case notFound = 404
    case ServerError = 500
    
}

struct Constants {
    static var id = Int()

}
