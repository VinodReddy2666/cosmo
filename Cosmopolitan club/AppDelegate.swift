//
//  AppDelegate.swift
//  Cosmopolitan club
//
//  Created by Vinod Reddy Sure on 05/06/19.
//  Copyright © 2019 Vinod Reddy Sure. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    private var reachability : Reachability?
    static let shared = AppDelegate()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

//        window?.rootViewController = ViewController()
//        window?.becomeKey()
//        window?.makeKeyAndVisible()
        DispatchQueue.global(qos: .background).async {
            self.startReachabilityChecking()
        }
        self.appearence()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func appearence() {
        UINavigationBar.appearance().barTintColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 0.9607843137, alpha: 1)
        UINavigationBar.appearance().tintColor = .white
        var attributes = [NSAttributedString.Key : Any]()
        attributes.updateValue(UIColor.white, forKey: .foregroundColor)
//        attributes.updateValue(UIFont(name: FontCustom.Bold.rawValue, size: 16.0)!, forKey : NSAttributedString.Key.font)
        UINavigationBar.appearance().titleTextAttributes = attributes
//        attributes.updateValue(UIFont(name:FontCustom.Medium.rawValue, size: 18.0)!, forKey : NSAttributedString.Key.font)
        if #available(iOS 11.0, *) {
            UINavigationBar.appearance().largeTitleTextAttributes = attributes
        }

    }


}
// MARK:- Reachability

extension AppDelegate {
    
//    // MARK:- Register Push
//    private func registerPush(forApp application : UIApplication){
//        let center = UNUserNotificationCenter.current()
//        center.requestAuthorization(options:[.alert, .sound]) { (granted, error) in
//
//            if granted {
//                DispatchQueue.main.async {
//                    application.registerForRemoteNotifications()
//                }
//            }
//        }
//    }
    
    // MARK:- Offline Booking on No Internet Connection
    
    func startReachabilityChecking() {
        
        self.reachability = Reachability()
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityAction), name: NSNotification.Name.reachabilityChanged, object: nil)
        //  self.reachability?.startNotifier()
        do {
            try self.reachability?.startNotifier()
        } catch let err {
            print("Error in Reachability", err.localizedDescription)
        }
    }
    
    
    func stopReachability() {
        // MARK:- Stop Reachability
        self.reachability?.stopNotifier()
    }
    
    // MARK:- Reachability Action
    
    @objc private func reachabilityAction(notification : Notification) {
        
        print("Reachability \(self.reachability?.connection.description ?? .Empty)", #function)
        guard self.reachability != nil else { return }
        if self.reachability!.connection == .none  {
            let offlineVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier:Storyboard.IDs.OfflineBookingViewController) as! OfflineBookingViewController
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = offlineVC
            
            
        } else {
            
        (UIApplication.topViewController() as? OfflineBookingViewController)?.dismiss(animated: true, completion: nil)
            
            
        }
    }
    
}



