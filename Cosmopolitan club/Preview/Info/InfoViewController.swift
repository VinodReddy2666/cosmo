//
//  InfoViewController.swift
//  Cosmopolitan club
//
//  Created by Vinod Reddy Sure on 10/06/19.
//  Copyright © 2019 Vinod Reddy Sure. All rights reserved.
//

import UIKit

class InfoViewController: UIViewController {

    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var placeLbl: UILabel!
    @IBOutlet weak var capacityLbl: UILabel!
    @IBOutlet weak var coversLbl: UIStackView!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var servicesTable: UITableView!
    @IBOutlet weak var servicesTableHeight: NSLayoutConstraint!
    @IBOutlet weak var cuisineTable: UITableView!
    @IBOutlet weak var cusineTableHeight: NSLayoutConstraint!
    
    @IBOutlet weak var BGScrollView: UIScrollView!
    @IBOutlet weak var BGView: UIView!

    
    var net = NetworkCalls()
    
    var facilitiesArray = NSMutableArray()
    var cuisinesArray = NSMutableArray()

    var headerView = UIView()
    let titleLbl = UILabel()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.servicesTable.separatorColor = UIColor.white
        self.cuisineTable.separatorColor = UIColor.white
        
        BGView.layer.shadowOffset =  CGSize(width: 0, height: 1)   // CGSizeMake(0, 1)
        BGView.layer.shadowColor = UIColor.black.cgColor
        BGView.layer.shadowRadius = 1.5
        BGView.layer.shadowOpacity = 0.65
        BGView.layer.cornerRadius = 1
        BGView.clipsToBounds = true
        BGView.layer.masksToBounds = false
        
        self.apiCall(api: Api.DiningDetails, url: "\(Api.DiningDetails.rawValue)\(Constants.id)")
        self.designHeader()
    }
    
    func designHeader() {
        titleLbl.text = "Cuisine"
        self.headerView.frame = CGRect(x: 9, y: 0, width: self.cuisineTable.frame.width, height: 30)
        titleLbl.frame = self.headerView.frame
        titleLbl.textColor = UIColor.black
        titleLbl.font = UIFont.systemFont(ofSize: 17, weight: .medium)
//        self.headerView.backgroundColor = UIColor.red
//        self.titleLbl.backgroundColor = UIColor.yellow
        self.headerView.addSubview(titleLbl)
        
    }
    override func viewDidAppear(_ animated: Bool) {

    }
    

    func apiCall(api: Api, url : String) {
        net.get(api: api, url: url, data: nil, type: HttpType.GET)
//        net.get(api: api, url: nil, data: nil, type: HttpType.GET)
        net.completion = { error,data in
            
            do
            {
                if api == Api.DiningDetails {
                    if let json = try JSONSerialization.jsonObject(with:
                        
                        data!, options: .mutableContainers) as? NSDictionary {
                        print("save addes places *lcn \(json)")
                        self.descriptionLbl.text = json.value(forKey: "description") as? String
                        self.nameLbl.text = json.value(forKey: "food_name") as? String
                        self.capacityLbl.text = "\(json.value(forKey: "capacity") as? Int ?? 0)"
                        self.placeLbl.text = "\(json.value(forKey: "location") as? String ?? "0"),\(json.value(forKey: "level") as? String ?? "0")"
                        
                        if json.value(forKey: "liquors_served") as? String == "yes" {
                            self.facilitiesArray.add("Liquor Served")
                        }
                        if json.value(forKey: "ac_available") as? String == "yes" {
                            self.facilitiesArray.add("AC Available")
                        }
                        if json.value(forKey: "tv_available") as? String == "yes" {
                            self.facilitiesArray.add("TV Available")
                        }
                        self.apiCall(api: Api.DiningCuisine, url: "\(Api.DiningCuisine.rawValue)\(Constants.id)")
                        self.servicesTable.delegate = self
                        self.servicesTable.dataSource = self
                        
                        self.servicesTableHeight.constant = CGFloat((self.facilitiesArray.count) * 30)
                        self.servicesTable.reloadData()
                        
                    }
                }else if api == Api.DiningCuisine {
                    if let json = try JSONSerialization.jsonObject(with:
                        
                        data!, options: .mutableContainers) as? NSArray {
                        print("save addes places *lcn \(json)")
                        self.cuisinesArray = json as! NSMutableArray
                        self.cuisineTable.delegate = self
                        self.cuisineTable.dataSource = self
                        self.cuisineTable.reloadData()
                        self.cusineTableHeight.constant = CGFloat(((self.cuisinesArray.count) * 30) + 5)
                        if self.cuisinesArray.count > 0 {
                            self.headerView.frame = CGRect(x: self.headerView.frame.origin.x, y: self.headerView.frame.origin.y, width: self.view.frame.width, height: 30)
                            self.cuisineTable.tableHeaderView = self.headerView
                        }else{
                            self.headerView.frame = CGRect(x: self.headerView.frame.origin.x, y: self.headerView.frame.origin.y, width: self.view.frame.width, height: 0)
                            self.cuisineTable.tableHeaderView = self.headerView
                        }
                        self.servicesTable.reloadData()
                        let height = (self.cuisinesArray.count * 30) + 10
                        
                        self.BGView.frame = CGRect(x: self.BGView.frame.origin.x, y: self.BGView.frame.origin.y, width: self.BGView.frame.width, height: self.cuisineTable.frame.origin.y + CGFloat(height))
                        self.BGScrollView.contentSize = CGSize(width: self.BGScrollView.frame.width, height: self.BGView.frame.origin.x + self.BGView.frame.height)
                        self.BGView.layer.shadowOffset =  CGSize(width: 0, height: 1)   // CGSizeMake(0, 1)
                        self.BGView.layer.shadowColor = UIColor.black.cgColor
                        self.BGView.layer.shadowRadius = 1.5
                        self.BGView.layer.shadowOpacity = 0.65
                        self.BGView.layer.cornerRadius = 1
                        self.BGView.clipsToBounds = true
                        self.BGView.layer.masksToBounds = false

                    }
                }
                
            } catch let error
            {
                print(error.localizedDescription)
            }
            
            
        }

    }
   
    

}

extension InfoViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == servicesTable  {
            return facilitiesArray.count
        }else{
            return cuisinesArray.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == servicesTable {
            let tableCell = servicesTable.dequeueReusableCell(withIdentifier: "FacilitiesTableViewCell", for: indexPath) as? FacilitiesTableViewCell
            tableCell?.facilitiNameLbk.text = self.facilitiesArray[indexPath.row] as? String
            tableCell?.selectionStyle = .none
            return tableCell!
        }else {
            let tableCell = cuisineTable.dequeueReusableCell(withIdentifier: "CuisineTableViewCell", for: indexPath) as? CuisineTableViewCell
            tableCell?.cuisineLbl.text = (self.cuisinesArray[indexPath.row] as! NSDictionary).value(forKey: "quisine_name") as? String
            tableCell?.selectionStyle = .none
            return tableCell!
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }

    
    
}
