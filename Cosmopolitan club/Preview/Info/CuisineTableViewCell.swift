//
//  CuisineTableViewCell.swift
//  Cosmopolitan club
//
//  Created by Vinod Reddy Sure on 11/06/19.
//  Copyright © 2019 Vinod Reddy Sure. All rights reserved.
//

import UIKit

class CuisineTableViewCell: UITableViewCell {

    @IBOutlet weak var cuisineLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
