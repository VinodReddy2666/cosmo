//
//  GalleryCollectionViewCell.swift
//  Cosmopolitan club
//
//  Created by Vinod Reddy Sure on 11/06/19.
//  Copyright © 2019 Vinod Reddy Sure. All rights reserved.
//

import UIKit

class GalleryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
}
