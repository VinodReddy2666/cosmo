//
//  GalleryCollectionViewController.swift
//  Cosmopolitan club
//
//  Created by Vinod Reddy Sure on 11/06/19.
//  Copyright © 2019 Vinod Reddy Sure. All rights reserved.
//

import UIKit
import Cache
private let reuseIdentifier = "GalleryCollectionViewCell"

class GalleryCollectionViewController: UICollectionViewController{

    var net = NetworkCalls()
    var galleryImage = NSArray()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.15, execute: {
            self.apiCall()

        })

        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: self.collectionView.frame.width/2, height: 127)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        collectionView!.collectionViewLayout = layout
        
        collectionView.delegate = nil
        collectionView.dataSource = nil

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
    }


    func apiCall() {
        net.get(api: .DiningImages, url: "\(Api.DiningImages.rawValue)\(Constants.id)", data: nil, type: HttpType.GET)
        net.completion = { error,data in
            
            do
            {
                if let json = try JSONSerialization.jsonObject(with:
                    
                    data!, options: .mutableContainers) as? NSArray {
                    print("save addes places *lcn \(json)")
                    self.galleryImage = json
                    self.collectionView.delegate = self
                    self.collectionView.dataSource = self
                    self.collectionView.reloadData()
                }
                
            } catch let error
            {
                print(error.localizedDescription)
            }
            
        }
    }
    
    

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return galleryImage.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! GalleryCollectionViewCell

        Cache.image(forUrl: "https:cosmopolitanclubchennai.com/images/food_dining/\(((self.galleryImage[indexPath.row]) as! NSDictionary).value(forKey: "image_name") as? String ?? "0")") { (image) in
            DispatchQueue.main.async {
                cell.image.contentMode = .scaleAspectFill
                cell.image.image = image == nil ? #imageLiteral(resourceName: "menu-button") : image
            }
        }

        return cell
    }
    
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.collectionView.frame.width / 2, height: 127)
    
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc=ImagePreviewVC()
        vc.imgArray = self.galleryImage
        vc.passedContentOffset = indexPath
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
//        self.navigationController?.present(vc, animated: true, completion: nil)
//        self.navigationController?.pushViewController(vc, animated: true)

    }
    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
