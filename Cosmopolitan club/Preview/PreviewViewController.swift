//
//  PreviewViewController.swift
//  Cosmopolitan club
//
//  Created by Vinod Reddy Sure on 05/06/19.
//  Copyright © 2019 Vinod Reddy Sure. All rights reserved.
//

import UIKit

class PreviewViewController: UIViewController,UIPageViewControllerDataSource, UIPageViewControllerDelegate, UIScrollViewDelegate {

    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var infoBtn: UIButton!
    @IBOutlet weak var galleryView: UIView!
    @IBOutlet weak var galleryBtn: UIButton!
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var buttonsView: UIView!
    
    var navTitle = String()
    var secondBoxSelected = Bool()
    
    private var pageController: UIPageViewController!
    private var arrVC:[UIViewController] = []
    private var currentPage: Int!
    
    var tab1VC:InfoViewController! = nil
    var tab2VC:GalleryCollectionViewController! = nil


    override func viewDidLoad() {
        super.viewDidLoad()
        self.switchViewAction()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.initialLoads()
    }
    
    
    func initialLoads() {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "left-arrow-1").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(self.backButtonClick))
        self.navigationItem.title = navTitle
        
        progressView.layer.shadowOffset =  CGSize(width: 0, height: 0.5)   // CGSizeMake(0, 1)
        progressView.layer.shadowColor = UIColor.black.cgColor
        progressView.layer.shadowRadius = 1.0
        progressView.layer.shadowOpacity = 0.65
        progressView.layer.cornerRadius = 1
        progressView.clipsToBounds = true
        progressView.layer.masksToBounds = false
        
        self.navigationController?.navigationBar.shouldRemoveShadow(true)
        self.infoBtn.backgroundColor = #colorLiteral(red: 0.1465961039, green: 0.5509909391, blue: 0.962056458, alpha: 1)
        self.galleryBtn.backgroundColor = #colorLiteral(red: 0.1465961039, green: 0.5509909391, blue: 0.962056458, alpha: 1)
        
        self.secondBoxSelected = false
        
        createPageViewController()

    }
    

    @IBAction func infoBtnAction(_ sender: Any) {
        
    }
    
    @IBAction func galleryBtnAction(_ sender: Any) {
        
    }
    
    private func switchViewAction(){
        // self.pastUnderLineView.isHidden = false
        // self.isFirstBlockSelected = true
        self.infoBtn.tag = 1
        self.galleryBtn.tag = 2
        self.infoBtn.addTarget(self, action: #selector(ButtonTapped(sender:)), for: .touchUpInside)
        self.galleryBtn.addTarget(self, action: #selector(ButtonTapped(sender:)), for: .touchUpInside)
    }
    
    @IBAction func ButtonTapped(sender: UIButton){
        
        self.secondBoxSelected = sender.tag == 1 ? false : true
        moving()
        animateUnderLine()
    }

    
    private func animateUnderLine() {
        if self.progressView != nil {
            UIView.animate(withDuration: 0.3) {
                let viewWidth = self.secondBoxSelected == true ? self.view.bounds.width/2 : 0.0
                self.progressView.frame.origin.x = viewWidth
            }
        }
    }
    
    func moving() {
        var selectedIndex = self.secondBoxSelected == true ? 1 : 0
        switch selectedIndex {
        case 0:
            
            pageController.setViewControllers([arrVC[0]], direction: UIPageViewController.NavigationDirection.reverse, animated: true, completion: nil)
            selectedIndex = 0
            
        case 1:
            if selectedIndex > 1{
                pageController.setViewControllers([arrVC[1]], direction: UIPageViewController.NavigationDirection.reverse, animated: true, completion: nil)
                selectedIndex = 1
            }else{
                pageController.setViewControllers([arrVC[1]], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: nil)
                selectedIndex = 1
                
            }
        case 2:
            if selectedIndex < 2 {
                pageController.setViewControllers([arrVC[2]], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: nil)
                selectedIndex = 2
                
                
            }else{
                pageController.setViewControllers([arrVC[2]], direction: UIPageViewController.NavigationDirection.reverse, animated: true, completion: nil)
                selectedIndex = 2
                
            }
        default:
            break
        }

    }
    
    private func createPageViewController() {

        pageController = UIPageViewController.init(transitionStyle: UIPageViewController.TransitionStyle.scroll, navigationOrientation: UIPageViewController.NavigationOrientation.horizontal, options: nil)
        
        pageController.view.backgroundColor = UIColor.clear
        pageController.delegate = self
        pageController.dataSource = self
        
        for svScroll in pageController.view.subviews as! [UIScrollView] {
            svScroll.delegate = self
        }
        let pageControllerY : CGFloat = self.navigationController!.navigationBar.frame.height + self.navigationController!.navigationBar.frame.origin.y + self.buttonsView.frame.height + self.buttonsView.frame.origin.y

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.pageController.view.frame = CGRect(x: 0, y: pageControllerY , width: self.view.frame.size.width, height: self.view.frame.height - pageControllerY)
        }
        
        let homeStoryboard = UIStoryboard(name: "Main", bundle: nil)
        tab1VC = homeStoryboard.instantiateViewController(withIdentifier: "InfoViewController") as? InfoViewController
        
        tab2VC = (homeStoryboard.instantiateViewController(withIdentifier: "GalleryCollectionViewController") as! GalleryCollectionViewController)
        
        arrVC = [tab1VC, tab2VC]
        pageController.setViewControllers([tab1VC], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
        
        self.addChild(pageController)
        self.view.addSubview(pageController.view)
        pageController.didMove(toParent: self)
    }
    
    
    private func indexofviewController(viewCOntroller: UIViewController) -> Int {
        if(arrVC .contains(viewCOntroller)) {
            return arrVC.index(of: viewCOntroller)!
        }
        
        return -1
    }
    
    //MARK: - Pagination Delegate Methods
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        var index = indexofviewController(viewCOntroller: viewController)
//        if index == 1 {
//            self.secondBoxSelected = true
//        }else{
//            self.secondBoxSelected = false
//        }
//        self.animateUnderLine()

        if(index != -1) {
            index = index - 1
        }
        
        if(index < 0) {
            return nil
        }
        else {
            return arrVC[index]
        }
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        var index = indexofviewController(viewCOntroller: viewController)
        if(index != -1) {
            index = index + 1
        }
        
        if(index >= arrVC.count) {
            return nil
        }
        else {
            
            return arrVC[index]
        }
        
    }
    
    func pageViewController(_ pageViewController1: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {

        if(completed) {

            currentPage = arrVC.index(of: (pageViewController1.viewControllers?.last)!)
            if currentPage == 1 {
                self.secondBoxSelected = true
            }else{
                self.secondBoxSelected = false
            }
            self.animateUnderLine()

        }
        
    }


}
extension UINavigationBar {
    
    func shouldRemoveShadow(_ value: Bool) -> Void {
        if value {
            self.setValue(true, forKey: "hidesShadow")
        } else {
            self.setValue(false, forKey: "hidesShadow")
        }
    }
}
