//
//  HomeViewController.swift
//  Cosmopolitan club
//
//  Created by Vinod Reddy Sure on 05/06/19.
//  Copyright © 2019 Vinod Reddy Sure. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
   
  //  @IBOutlet weak var loginView: UIView!
    var isFrom = String()
    var index : Int?
    
    @IBOutlet weak var memberCodeText: HoshiTextField!
    @IBOutlet weak var dobText: HoshiTextField!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var listTableView: UITableView!
    var loginView : LoginView?
    var loginVal : Int = 0
    var net = NetworkCalls()
    let menuItems = ["Home","Member Login","Bill Summary","Payments History","Bookings","Library","Dining","Banquets","Facilities","Sports","Golf Annexe","Events","Promotions","Gallery","Notifications"]
    
    let keysForHome = [String]()
    let keysForMemberLogIn = [String]()
    let keysForBill = [String]()
    let keysForPayments = [String]()
    let keysForBookings = [String]()
    let keysForLibrary = [String]()
    let keysForDining = ["food_name","location","level","capacity"]
    let keysForBanquets = ["banquets_name","location","hall_level","hall_capacity"]
    let keysForFacilities = ["facility_name","location","level","hall_capacity"]
    let keysForSports = ["sports_name","location","hall_level","hall_capacity"]
    let keysForGolf = ["gf_name","location","hall_level","hall_capacity"]
    let keysForEvents = ["facility_name","location","hall_level","hall_capacity"]
    let keysForPromotions = ["facility_name","location","hall_level","hall_capacity"]
    let keysForGallery = ["facility_name","location","hall_level","hall_capacity"]
    let keysForNotifications = [String]()
    
    let imageKeys = ["Home","Member Login","Bill Summary","Payments History","Bookings","Library","food_dining/","hall_images/","facility/","sports/","golf_annex/","events/","promotions/","gallery/","Notifications"]

    var keysArray = [String]()
    var response = NSArray()
    var responseDictionary = NSDictionary()
    var responseKeys = [Int]()
    var apiArray = [Api]()
    var banquetsResponse = NSArray()
    
    
    lazy var loader  : UIView = {
        return createActivityIndicator(self.view)
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialLoads()
    }
    
    func initialLoads() {
        
        apiArray = [Api.home,Api.memberLogIn,Api.billSummary,Api.paymentHistory,Api.bookings,Api.library,Api.DiningList,Api.BanquetList,Api.FacilityList,Api.SportsList,Api.GolfList,Api.EventsList,Api.PromotionsList,Api.GalleryList,Api.Notifications]

        self.listTableView.separatorColor = UIColor.white
        self.navigationController?.isNavigationBarHidden = false
        let menuBtn = UIBarButtonItem(image:  #imageLiteral(resourceName: "menu-button").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector((self.menuBtnAction)))
        menuBtn.target = revealViewController()
        menuBtn.action = #selector(SWRevealViewController.revealToggle(_:))
        self.view.addGestureRecognizer((self.revealViewController()?.panGestureRecognizer())!)
        self.navigationItem.leftBarButtonItem = menuBtn
        self.navigationItem.title = menuItems[index!]
        if index == 0 || index == 1 || index == 2 || index == 3 || index == 4 || index == 5 {
            
            self.showLoginView()
            
        }else if index == 6 || index == 7 || index == 8 || index == 9 || index == 10 {
            self.loader.isHidden = false
            self.apiCall()
        }else{
            
        }
    }
    
    
    
    @objc func menuBtnAction(sender : UIBarButtonItem) {
        sender.target = revealViewController()
        sender.action = #selector(SWRevealViewController.revealToggle(_:))
    }
    
    @IBAction func nextView(_ sender: Any) {
        
        let previewVC = self.storyboard?.instantiateViewController(withIdentifier: "PreviewViewController") as? PreviewViewController
        self.navigationController?.pushViewController(previewVC!, animated: true)
    }
    
    @IBAction func loginAction(_ sender: Any) {
        
        
        
    }
    
    private func showLoginView(){
        
        if self.loginView == nil, let signInView = Bundle.main.loadNibNamed(XIB.IDs.LoginView, owner: self, options: [:])?.first as? LoginView {
            loginView = signInView
            self.loginView?.frame = self.view.frame

            self.loginView?.getSignInValues = { memNumber,dob in
            
            }
            self.view.addSubview(self.loginView!)
        }
    }
    
    
    private func removeSingInView(){
        
        self.loginView?.dismissView(onCompletion: {
            self.loginView = nil
           
        })
        
    }
    
    func apiCall() {
        
        net.get(api: apiArray[index!], url: nil, data: nil, type: HttpType.GET)
        net.completion = { error,data in
            
            do
            {
//                if self.apiArray[self.index!] == Api.DiningList {
//                    if let json = try JSONSerialization.jsonObject(with:
//
//                        data!, options: .mutableContainers) as? NSDictionary {
//                        print("save addes places *lcn \(json)")
//                        //                    self.response = [json]
//                        print(json.allKeys)
//                        self.responseDictionary = json
//                        self.responseKeys = (json.allKeys as! [String]).map { Int($0)!}
//                        self.responseKeys = self.responseKeys.sorted(by: { $0 < $1 })
//                        self.keysArray = self.keysForDining
//                        self.listTableView.reloadData()
//                    }
//                }else  {
                    if let json = try JSONSerialization.jsonObject(with:
                        
                        data!, options: .mutableContainers) as? NSArray {
                        print("save addes places *lcn \(json)")
                        self.banquetsResponse = json
                        if self.apiArray[self.index!] == Api.DiningList{
                            self.keysArray = self.keysForDining
                        }
                        else if self.apiArray[self.index!] == Api.BanquetList {
                            self.keysArray = self.keysForBanquets
                        }else if self.apiArray[self.index!] == Api.FacilityList {
                            self.keysArray = self.keysForFacilities
                        }else if self.apiArray[self.index!] == Api.SportsList {
                            self.keysArray = self.keysForSports
                        }else if self.apiArray[self.index!] == Api.GolfList {
                            self.keysArray = self.keysForGolf
                        }
                        
                        var set = Set<String>()
                        let arraySet: [NSDictionary] = self.banquetsResponse.flatMap {
                            guard let name = ($0 as! NSDictionary).value(forKey: self.keysArray[0]) as? String else {return nil }
                            return set.insert(name).inserted ? $0 as? NSDictionary : nil
                        }
                        print(set)
                        print(arraySet)
                        self.banquetsResponse = arraySet as NSArray
//                    }
                    
                    self.listTableView.reloadData()
                    
                }
                self.loader.isHidden = true

            } catch let error
            {
                print(error.localizedDescription)
            }
            
        }

    }
    
}

extension HomeViewController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        apiArray[index!] == Api.DiningList ? (responseKeys.count > 0 ? responseKeys.count : 0) :
        return  banquetsResponse.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tableCell = listTableView.dequeueReusableCell(withIdentifier: "ListTableViewCell", for: indexPath) as? ListTableViewCell
        
        
        tableCell!.BGView.layer.shadowOffset =  CGSize(width: 0, height: 1)   // CGSizeMake(0, 1)
        tableCell!.BGView.layer.shadowColor = UIColor.black.cgColor
        tableCell!.BGView.layer.shadowRadius = 1.5
        tableCell!.BGView.layer.shadowOpacity = 0.65
        tableCell!.BGView.layer.cornerRadius = 1
        tableCell!.BGView.clipsToBounds = true
        tableCell!.BGView.layer.masksToBounds = false
        tableCell!.selectionStyle = .none
        
        tableCell!.nameLbl.text =  (banquetsResponse[indexPath.row] as! NSDictionary).value(forKey: keysArray[0]) as? String
//        apiArray[index!] == Api.DiningList ? (responseDictionary.value(forKey: "\(responseKeys[indexPath.row])") as! NSDictionary).value(forKey: keysArray[0]) as? String :
        tableCell!.subTitle.text = "\((banquetsResponse[indexPath.row] as! NSDictionary).value(forKey: keysArray[1]) as? String ?? ""),\((banquetsResponse[indexPath.row] as! NSDictionary).value(forKey: keysArray[2]) as? String ?? "")"
//apiArray[index!] == Api.DiningList ? "\((responseDictionary.value(forKey: "\(responseKeys[indexPath.row])") as! NSDictionary).value(forKey: keysArray[1]) as? String ?? "00"),\((responseDictionary.value(forKey: "\(responseKeys[indexPath.row])") as! NSDictionary).value(forKey: keysArray[2]) as? String ?? "00")" :
        if (tableCell!.subTitle.text)?.last == "," {
            let str = tableCell!.subTitle.text
            let str1 = str!.substring(to: str!.index(before: str!.endIndex))
            tableCell!.subTitle.text = str1
        }
        
        
        
        if apiArray[index!] == Api.DiningList ||  apiArray[index!] == Api.BanquetList {
            tableCell!.capacity.text =  "\((banquetsResponse[indexPath.row] as! NSDictionary).value(forKey: keysArray[3]) as? Int ?? 0)"
            tableCell!.covers.text = "Covers"
        }else{
            tableCell!.capacity.text = ""
            tableCell!.covers.text = ""
            
        }
        let url = "\(imageBaseUrl)\(self.imageKeys[self.index!])\((banquetsResponse[indexPath.row] as! NSDictionary).value(forKey: "image_name") as? String ?? "00")"
        Cache.image(forUrl: url) { (image) in
            DispatchQueue.main.async {
                tableCell!.displayImage.contentMode = .scaleAspectFit
                tableCell!.displayImage.image = image == nil ? #imageLiteral(resourceName: "menu-button") : image
            }
        }


        return tableCell!

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PreviewViewController") as! PreviewViewController
        vc.navTitle = ((banquetsResponse[indexPath.row] as! NSDictionary).value(forKey: keysArray[0]) as? String)!
        Constants.id = (banquetsResponse[indexPath.row] as! NSDictionary).value(forKey: "food_dining_id") as! Int

        self.navigationController?.pushViewController(vc, animated: true)
    }
}
extension HomeViewController : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        (textField as? HoshiTextField)?.borderActiveColor = .primary
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        (textField as? HoshiTextField)?.borderActiveColor = .lightGray
    }
}
