//
//  ListTableViewCell.swift
//  Cosmopolitan club
//
//  Created by Vinod Reddy Sure on 07/06/19.
//  Copyright © 2019 Vinod Reddy Sure. All rights reserved.
//

import UIKit

class ListTableViewCell: UITableViewCell {

    @IBOutlet weak var BGView: UIView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var capacity: UILabel!
    @IBOutlet weak var covers: UILabel!
    @IBOutlet weak var displayImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
