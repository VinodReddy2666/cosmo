//
//  NetworkCalls.swift
//  Cosmopolitan club
//
//  Created by Chan Basha Shaik on 07/06/19.
//  Copyright © 2019 Vinod Reddy Sure. All rights reserved.
//

import Foundation
import Alamofire


class NetworkCalls {
    
var completion : ((CustomError?, Data?)->())?

func retrieve(api: Api,url : String?, data: Data?, imageData: [String:Data]?, paramters: [String : Any]?, type: HttpType, completion : ((CustomError?, Data?)->())?) {
    
    print("To url ", api.rawValue)
    
    if data != nil {
        
        print("\nAt Webservice Request  ",String(data: data!, encoding: .utf8) ?? .Empty)
    }
    if paramters != nil {
        
        print("\nAt Webservice Request  ",paramters!)
    }
    
    if imageData != nil {
        
        print("\nImage Data Request  ",imageData!)
    }
    
    self.completion = completion

    if url != nil { // send normal GET POST call
        
        self.send(api: api,url : url!, data: data, parameters: paramters, type: type)
        
    } else {
        
        self.send(api: api,url : nil, data: data, parameters: paramters, type: type)
    }
    
    }
    

    func send(api: Api, url appendingUrl : String?, data: Data?, parameters : [String : Any]?, type : HttpType) {
        
        var url : URL?
        var urlRequest : URLRequest?
        var getParams : String = .Empty
        
        switch type {
            
        case .GET:
            
            if appendingUrl != nil {
                
                getParams = appendingUrl!
                
            }else {
                
                for (index,param) in (parameters ?? [:]).enumerated() {
                    
                    getParams.append((index == 0 ? "?" : "&")+"\(param.key)=\(param.value)")
                    
                }
                
                getParams = api.rawValue+getParams
            }
            
            
            
            
        case .POST:
            
            if appendingUrl == nil {
                getParams = api.rawValue.trimmingCharacters(in: .whitespaces)
            } else {
                getParams = appendingUrl!
            }
            
        case .DELETE, .PATCH, .PUT :
            
            getParams = appendingUrl ?? .Empty
            
            
        }
        
        
        url = URL(string: baseUrl+getParams ) // Setting Base url as FCM incase of FCM Push
        
        if url != nil {
            urlRequest = URLRequest(url: url!)
        }
        
        urlRequest?.httpBody = data
        urlRequest?.httpMethod = type.rawValue
        
        
        guard urlRequest != nil else { // Flow validation in url request
//            interactor?.on(api: api, error: CustomError(description: ErrorMessage.list.serverError, code: StatusCode.ServerError.rawValue))
            return
        }
        
        
        // Setting Secret Key to Identify the response Api
        
        urlRequest?.addValue(api.rawValue, forHTTPHeaderField: WebConstants.string.secretKey)
        urlRequest?.addValue(WebConstants.string.application_json, forHTTPHeaderField: WebConstants.string.Content_Type)
        urlRequest?.addValue(WebConstants.string.XMLHttpRequest, forHTTPHeaderField: WebConstants.string.X_Requested_With)
//        urlRequest?.addValue(WebConstants.string.bearer+String.removeNil(User.main.accessToken), forHTTPHeaderField: WebConstants.string.Authorization)
        
        Alamofire.request(urlRequest!).validate(statusCode: StatusCode.success.rawValue..<StatusCode.multipleResponse.rawValue).responseJSON { (response) in
            let api = response.request?.value(forHTTPHeaderField: WebConstants.string.secretKey) ?? .Empty
            switch response.result{
                
            case .failure(let err):
                print("At Webservice Response  at ",api,"   ",err, response.response?.statusCode ?? 0)
            case .success(let val):
                print("At Webservice Response ",api,"   ",val, response.response?.statusCode ?? 0)
            }
            
            self.send(response)
            
        }
    }
    
    fileprivate func send(_ response: (DataResponse<Any>)?) {
        
        if let data = response?.data {  // Validation For Server Data
            
            self.completion?(nil, data)
            
            
        }  else { // Validation for Exceptional Cases
            
            self.completion?(CustomError(description: ErrorMessage.list.serverError, code: StatusCode.ServerError.rawValue), nil)
           
            
        }
        
    }
    
    func get(api: Api, url: String?, data: Data?, type: HttpType) {
        
       retrieve(api: api,url: url, data: data, imageData: nil, paramters: nil, type: type, completion: nil)
        
    }
    
    
    func post(api: Api, data: Data?, paramters: [String : Any]?, type: HttpType) {
        retrieve(api: api,url: nil, data: data, imageData: nil, paramters: paramters, type: type, completion: nil)
    }
    
    
    func post(api: Api, imageData: [String : Data]?, parameters: [String : Any]?) {
        retrieve(api: api,url: nil, data: nil, imageData: imageData, paramters: parameters, type: .POST, completion: nil)
    }
    
}
