//
//  ViewController.swift
//  Cosmopolitan club
//
//  Created by Vinod Reddy Sure on 05/06/19.
//  Copyright © 2019 Vinod Reddy Sure. All rights reserved.
//

import UIKit
import ZKCarousel

class ViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var menuBrn: UIBarButtonItem!
    
    @IBOutlet weak var carouselView: ZKCarousel!
    
    @IBOutlet weak var facilitiesCV: UICollectionView!
  
    @IBOutlet weak var bgView1: UIView!
    var imagesArray = ["2","2","2","2"]
    var titlesArray = ["Dining","Banquets","Sports","Facilities"]
    var descArray = ["Cinnamon","Board Room","Squash","Cards Room"]
    
    @IBOutlet weak var imageView1: UIImageView!
    @IBOutlet weak var imageView2: UIImageView!
    @IBOutlet weak var imageView3: UIImageView!
    
    @IBOutlet weak var imageView4: UIImageView!
    
    
    
    
    @IBOutlet weak var labelDining: UILabel!
    @IBOutlet weak var labelCinnamon: UILabel!
    @IBOutlet weak var labelBanuuets: UILabel!
    @IBOutlet weak var labelBaordRoom: UILabel!
    
    @IBOutlet weak var labelSports: UILabel!
    
    @IBOutlet weak var viewBGForMenu: UIView!
    @IBOutlet weak var labelFacilities: UILabel!
    
    @IBOutlet weak var labelCardRoom: UILabel!
    @IBOutlet weak var labelSquash: UILabel!
    @IBOutlet var contentView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.initialLoads()
    }
    func initialLoads() {
        
        self.menuBrn.target = revealViewController()
        self.menuBrn.action = #selector(SWRevealViewController.revealToggle(_:))
        self.view.addGestureRecognizer((self.revealViewController()?.panGestureRecognizer())!)
        self.navigationItem.title = "Cosmopolitan Club"
        self.scrollView.addSubview(contentView)
        self.scrollView.contentOffset = .zero
        self.carouselView.layer.cornerRadius = 10
        self.carouselView.layer.shadowRadius = 1.5
//        self.carouselView.layer.shadowOffset = 1
        self.carouselView.layer.shadowColor = UIColor.gray.cgColor
        
        facilitiesCV.register(UINib(nibName: XIB.IDs.FacilitiesCell, bundle: nil), forCellWithReuseIdentifier: XIB.IDs.FacilitiesCell)

        carsoulSetup()
        setLayout()
        //setFont()
        setConstants()
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.contentView.frame = self.scrollView.bounds
        self.scrollView.contentSize = self.contentView.bounds.size
    }
   
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    private func setLayout(){
        
        //self.bgView1.backgroundColor = .red
        self.bgView1.layer.cornerRadius = 6
        self.bgView1.layer.shadowRadius = 1
        self.bgView1.layer.shadowColor = UIColor.gray.cgColor
        self.bgView1.layer.masksToBounds = true
        self.imageView1.layer.cornerRadius = 5
        self.imageView1.layer.masksToBounds = true
        self.imageView2.layer.cornerRadius = 5
        self.imageView2.layer.masksToBounds = true
        self.imageView3.layer.masksToBounds = true
        self.imageView3.layer.cornerRadius = 5
        self.imageView4.layer.masksToBounds = true
        self.imageView4.layer.cornerRadius = 5
        
//        bgView1.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: 1, height: 1), radius: 3, scale: true)

        bgView1.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: 1, height: 1), radius: 3, scale: true)
        viewBGForMenu.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: 1, height: 1), radius: 3, scale: true)
        
        //bgView1.layer.cornerRadius = 4
        
        // border
        //bgView1.layer.borderWidth = 1.0
        //bgView1.layer.borderColor = UIColor.gray.cgColor
        
        // shadow
//        bgView1.layer.shadowColor = UIColor.gray.cgColor
//        bgView1.layer.shadowOffset = CGSize(width: 1, height: 1)
//        bgView1.layer.shadowOpacity = 0.7
//        bgView1.layer.shadowRadius = 1
        
    }
    
    private func setFont(){
        
        
        Common.setFont(to: labelDining, isTitle: true, size: 10)
        Common.setFont(to: labelCinnamon, isTitle: false, size: 8)
        Common.setFont(to: labelBanuuets, isTitle: true, size: 10)
        Common.setFont(to: labelBaordRoom, isTitle: false, size: 8)
        Common.setFont(to: labelSports, isTitle: true, size: 10)
        Common.setFont(to: labelSquash, isTitle: false, size: 8)
        Common.setFont(to: labelFacilities, isTitle: true, size: 10)
        Common.setFont(to: labelCardRoom, isTitle: true, size: 8)

        
        
    }
    
    
    
    private func setConstants(){
        
            labelDining.text = "Dining"
            labelCinnamon.text = "Cinnamon"
            labelBanuuets.text = "Banquests"
            labelBaordRoom.text = "Baord Room"
            labelSports.text = "Sports"
            labelSquash.text = "Squash"
            labelFacilities.text = "Facilities"
            labelCardRoom.text = "Card Room"
 
        
    }
    
    private func carsoulSetup(){
        
        
        for index in 0..<self.imagesArray.count {
            
            let pic = ZKCarouselSlide(image:UIImage(named:imagesArray[index])!, title: "", description: "")
            self.carouselView.slides.append(pic)

        }
        self.carouselView.interval = 1.5
        self.carouselView.start()
        
    }

}

extension ViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:XIB.IDs.FacilitiesCell, for: indexPath) as! FacilitiesCell
        cell.imageFacilities.image = UIImage(imageLiteralResourceName:imagesArray[indexPath.row])
        cell.labelTitle.text = self.titlesArray[indexPath.row]
        cell.labelDesc.text = self.descArray[indexPath.row]
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 2, height: 130)
    }
    
}
