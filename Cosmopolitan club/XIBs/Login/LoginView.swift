//
//  LoginView.swift
//  Cosmopolitan club
//
//  Created by Chan Basha Shaik on 06/06/19.
//  Copyright © 2019 Vinod Reddy Sure. All rights reserved.
//

import UIKit

class LoginView: UIView {

    @IBOutlet weak var textfieldMemberCode: HoshiTextField!
    
    @IBOutlet weak var textfieldDOB: HoshiTextField!
    
    @IBOutlet weak var buttonLogin: UIButton!
    
    var getSignInValues : ((String?,String?)->Void)?
    
    
    override func awakeFromNib() {
        
         setFont()
        self.buttonLogin.addTarget(self, action: #selector(sendBackValues(sender:)), for: .touchUpInside)
        
    }
    
    private func setFont(){
        
      Common.setFont(to: buttonLogin, isTitle: true, size: 16)
      Common.setFont(to: textfieldMemberCode, isTitle: true, size: 14)
      Common.setFont(to: textfieldDOB, isTitle: true, size: 14)


    }
    
    @IBAction func sendBackValues(sender:UIButton){
        
      guard let memberCode = self.textfieldMemberCode.text, !memberCode.isEmpty
        else {
        return self.make(toast: "Please Enter Your Member Code")
      }
        guard let dob = self.textfieldDOB.text, !memberCode.isEmpty
            else {
            return self.make(toast: "Please Enter Your DOB")
        }
       
        getSignInValues?(memberCode,dob)
        
    }
    
    
    
}
