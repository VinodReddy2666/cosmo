//
//  FacilitiesCell.swift
//  Cosmopolitan club
//
//  Created by Chan Basha Shaik on 15/06/19.
//  Copyright © 2019 Vinod Reddy Sure. All rights reserved.
//

import UIKit

class FacilitiesCell: UICollectionViewCell {

    @IBOutlet weak var imageFacilities: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDesc: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
       setFont()
    self.imageFacilities.layer.cornerRadius = 4
    }
    
    private func setFont(){
        
        Common.setFont(to: labelTitle, isTitle: true, size: 12)
        Common.setFont(to: labelDesc, isTitle: true, size: 10)

    }

}
