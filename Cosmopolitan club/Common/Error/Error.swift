//
//  Error.swift
//  Cosmopolitan club
//
//  Created by Chan Basha Shaik on 07/06/19.
//  Copyright © 2019 Vinod Reddy Sure. All rights reserved.
//

import Foundation

// Custom Error Protocol
protocol CustomErrorProtocol : Error {
    var localizedDescription : String {get set}
}


// Custom Error Struct for User Defined Error Messages

struct CustomError : CustomErrorProtocol {
    
    var localizedDescription: String
    var statusCode : Int
    
    init(description : String, code : Int){
        self.localizedDescription = description
        self.statusCode = code
    }
}
struct ErrorMessage {
    
    static let list = ErrorMessage()
    
    let serverError = "Server Could not be reached. \n Try Again"
    let notReachable = "The Internet connection appears to be offline."
    let enterEmail = "Enter Email Id"
    let enterValidEmail = "Enter Valid Email Id"
    let enterPassword = "Enter Password"
    let enterFirstName = "Enter FirstName"
    let enterLastName = "Enter LastName"
    let enterMobileNumber = "Enter Mobile Number"
    let enterCountry = "Enter Country"
    let enterTimezone = "Enter TimeZone"
    let enterConfirmPassword = "Enter Confirm Password"
    let passwordDonotMatch = "Password and Confirm password donot match"
}
struct ErrorLogger : Decodable {
    
    var error : String?
}

struct WebConstants {
    
    static let string = WebConstants()
    
    let post = "POST"
    let secretKey = "secretKey"
    let X_Requested_With = "X-Requested-With"
    let XMLHttpRequest = "XMLHttpRequest"
    let Content_Type = "Content-Type"
    let application_json = "application/json"
    let multipartFormData = "multipart/form-data"
    let Authorization = "Authorization"
    let access_token = "access_token"
    let CFBundleShortVersionString = "CFBundleShortVersionString"
    let password = "password"
    let bearer = "Bearer "
    let picture = "picture"
    let http = "http"
}
