//
//  Identifiers.swift
//  Cosmopolitan club
//
//  Created by Chan Basha Shaik on 06/06/19.
//  Copyright © 2019 Vinod Reddy Sure. All rights reserved.
//

import Foundation

//Mark:- Storyboard identifiers

struct Storyboard {
    
    static let IDs = Storyboard()
    let OfflineBookingViewController = "OfflineBookingViewController"
    
    
}

struct XIB {
    
    static let IDs = XIB()
    let LoginView = "LoginView"
    let imagePreview = "imagePreview"
    let FacilitiesCell = "FacilitiesCell"
    
    
    
    

}
