//
//  MenuViewController.swift
//  Cosmopolitan club
//
//  Created by Vinod Reddy Sure on 05/06/19.
//  Copyright © 2019 Vinod Reddy Sure. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {

    let menuItems = ["Home","Member Login","Bill Summary","Payments History","Bookings","Library","Dining","Banquets","Facilities","Sports","Golf Annexe","Events","Promotions","Gallery","Notifications"]
    @IBOutlet weak var menuTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.iniialLoads()
    }
    
    func iniialLoads() {
        self.menuTableView.separatorColor = UIColor.white
        
    }
    


}

extension MenuViewController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tableCell = menuTableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell", for: indexPath) as? MenuTableViewCell
        tableCell?.menuLbl.text = self.menuItems[indexPath.row]
        tableCell?.selectionStyle = .none
        tableCell?.menuImageView.image = UIImage(named: menuItems[indexPath.row] as! String)
        return tableCell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            let newFrontViewController = UINavigationController.init(rootViewController: vc)
            revealViewController().pushFrontViewController(newFrontViewController, animated: true)
        }else{
            guard indexPath.row == 0 || indexPath.row == 6 else {
                return
            }
            let revealViewController : SWRevealViewController = self.revealViewController()
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            vc.isFrom = self.menuItems[indexPath.row]
            vc.index = indexPath.row
            let newFrontViewController = UINavigationController.init(rootViewController: vc)
            revealViewController.pushFrontViewController(newFrontViewController, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
}
